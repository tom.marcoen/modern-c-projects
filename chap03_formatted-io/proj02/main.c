#include <stdio.h>

int
main(void) {
	int item;
	float price;
	int month;
	int day;
	int year;

	printf("Enter item number: ");
	scanf("%d", &item);
	printf("Enter unit price: ");
	scanf("%f", &price);
	printf("Enter purchase date (mm/dd/yyyy): ");
	scanf("%d/%d/%d", &month, &day, &year);

	printf("%-20s  %-20s  Purchase\n", "Item", "Unit");
	printf("%-20s  %-20s  Date\n", "", "Price");
	printf("%-20d  $%7.2f%12s  %2d/%2d/%4d\n", item, price, "", month, day, year);
	return 0;
}
