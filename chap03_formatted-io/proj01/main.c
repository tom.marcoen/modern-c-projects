#include <stdio.h>

int
main(void) {
	int month;
	int day;
	int year;

	printf("Enter a date (mm/dd/yyyy): ");
	scanf("%2d/%2d/%4d", &month, &day, &year);

	printf("You entered the date %2d%02d%02d\n", year, month, day);
	return 0;
}
