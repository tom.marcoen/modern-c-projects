/* 3.2 The scanf function
 *
 * This program started out as the addfrac.c program from pp. 46-47.
 * It adds two fractions together.
 */
#include <stdio.h>

int
main(void) {
	int num1, denom1, num2, denom2, result_num, result_denom;

	printf("Enter first fraction: ");
	scanf("%d/%d", &num1, &denom1);

	printf("Enter second fraction: ");
	scanf("%d/%d", &num2, &denom2);

	result_num = num1 * denom2 + num2 * denom1;
	result_denom = denom1 * denom2;

	printf("The sum is %d/%d\n", result_num, result_denom);

	return 0;
}
