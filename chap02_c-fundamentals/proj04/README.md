# Project 02-04

Page: 34

Write a program that asks the user to enter a dollars-and-cents amount, then display the amount with 5% tax added:

Enter an amount: 100.00
With tax added: $105.00

This exercise has a solution provided online at http://knking.com/books/c2/.

## Solution
