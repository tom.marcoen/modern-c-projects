#include <stdio.h>

int
main(void) {
	int dollar;
	int twenties;
	int tens;
	int fives;

	printf("Enter a dollar amount: ");
	scanf("%d", &dollar);

	/* Twenty-dollar bills */
	twenties = dollar / 20;
	dollar = dollar - twenties * 20;
	printf("$20 bills: %-2d\n", twenties);

	/* Ten-dollar bills */
	tens = dollar / 10;
	dollar = dollar - tens * 10;
	printf("$10 bills: %-2d\n", tens);

	/* Five-dollar bills */
	fives = dollar / 5;
	dollar = dollar - fives * 5;
	printf(" $5 bills: %-2d\n", fives);

	/* One-dollar bills */
	printf(" $1 bills: %-2d\n", dollar);

	return 0;
}
