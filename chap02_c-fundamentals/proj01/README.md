# Project 02-01

Page: 34

Write a program that uses printf to display the following picture on the screen:

'''
       *
      *
     *
*   *
 * *
  *
'''

## Solution

This project is really easy to complete when you surround the desired output with printf() statements.
