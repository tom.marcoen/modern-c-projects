#include <stdio.h>
#include <math.h>

int
main(void) {
	int radius = 10;
	float volume;
	printf("Enter radius: ");
	scanf("%d", &radius);
	volume = 4.0f/3.0f * radius * radius * radius * M_PI;
	printf("Radius: %d", radius);
	printf(" -- Volume: %.3f\n", volume);
	return 0;
}
