# Project 02-02

Page: 34

Modify the program of project 2 so that it prompts the user to enter the radius of the sphere.

## Project 2 exercise

Write a program that computes the volume of a sphere with a 10-meter radius, using the formula V = 4/3 * Pi * r^3. Write the fraction 4/3 as 4.0f/3.0f. (Try writing it as 4/3. What happens?) Hint: C does not have an exponentation operator, so you will need to multiply r by itself twice to computer r^3.

## Solution
