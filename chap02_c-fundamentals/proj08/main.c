#include <stdio.h>

int
main(void) {
	float loan;
	float interest;
	float payments;

	float monthly;
	float balance;

	printf("Enter amount of loan: ");
	scanf("%f", &loan);
	printf("Enter interest rate: ");
	scanf("%f", &interest);
	printf("Enter monthly payment: ");
	scanf("%f", &payments);

	monthly = 1 + (interest / 100) / 12;

	printf("\nBalance remaining after first payment: ");
	balance = loan * monthly;
	balance -= payments;
	printf("$%.2f\n", balance);

	printf("Balance remaining after second payment: ");
	balance *= monthly;
	balance -= payments;
	printf("$%.2f\n", balance);

	printf("Balance remaining after third payment: ");
	balance *= monthly;
	balance -= payments;
	printf("$%.2f\n", balance);

	return 0;
}
