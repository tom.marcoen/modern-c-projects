#include <stdio.h>

float power(float x, int p);

int
main(void) {
	float x;
	printf("Enter a value for x: ");
	scanf("%f", &x);

	printf("Computing 3x^5 + 2x^4 - 5x^3 - x^2 + 7x - 6... ");
	float solution =
		3 * power(x,5)
		+ 2 * power(x,4)
		- 5 * power(x,3)
		- 1 * power(x,2)
		+ 7 * power(x,1)
		- 6 * power(x,0);
	printf("%.3f\n", solution);
	return 0;
}

float power(float x, int p) {
	if (p == 1) return x;
	if (p == 0) return 1;
	if (p < 0) return 0;  /* Let's not go here. */

	return x * power(x, p-1);
}
