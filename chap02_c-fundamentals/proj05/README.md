# Project 02-05

page: 34

Write a program that asks the user to enter a value for x and then displays the value of the following polynomial:

3x^5 + 2x^4 - 5x^3 - x^2 + 7x - 6

Hint: C does not have a exponentiation operator, so you will need to multiply x by itself repeatedly in order to compute the powers of x.
(For example, x * x * x is x cubed.)

## Solution

I will use a function, which we will learn about in chapter 9, "Functions". More so, I will use a recursive function to compute the powers of x.
