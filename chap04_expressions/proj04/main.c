#include <stdio.h>

int
main(void) {
	int number;
	printf("Enter a number between 0 and 32767: ");
	scanf("%d", &number);
	printf("In octal, your number is 0%o\n", number);

	printf("Now with calculating the number ourself: 0");
	int d;
	while ((d = number / 8) > 0) {
		printf("%d", d);
		number -= d * 8;
	}
	printf("%d\n", number);
	return 0;
}
