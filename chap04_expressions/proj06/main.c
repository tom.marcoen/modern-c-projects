#include <stdio.h>

int
main(void) {
   int d[12];
   printf("Enter the first 12 digits of an EAN: ");
   scanf("%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d",
      &d[0], &d[1], &d[2],
      &d[3], &d[4], &d[5],
      &d[6], &d[7], &d[8],
      &d[9], &d[10], &d[11]);
   int s1 = d[1] + d[3] + d[5] + d[7] + d[9] + d[11];
   int s2 = d[0] + d[2] + d[4] + d[6] + d[8] + d[10];
   s2 += 3 * s1;
   s2--;
   int r = s2 % 10;
   int checksum = 9 - r;
   printf("Check digit: %d\n", checksum);

   return 0;
}
